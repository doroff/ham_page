const gallery = document.querySelector('.work-images');
const loadMoreBtn = document.querySelector('.load-more');
const imagesToLoad = 12; 
const imageUrls = [ 
  './img/web-design1.jpg',
  './img/graphic-design1.jpg',
  './img/landing-page1.jpg',
  './img/wordpress1.jpg',
  './img/web-design2.jpg',
  './img/graphic-design2.jpg',
  './img/landing-page2.jpg',
  './img/wordpress2.jpg',
  './img/web-design3.jpg',
  './img/graphic-design3.jpg',
  './img/landing-page3.jpg',
  './img/wordpress3.jpg',
];

let loadedImages = 0;

function loadMoreImages() {
  for (let i = 0; i < imagesToLoad; i++) {
    if (loadedImages >= imageUrls.length) {
      loadMoreBtn.style.display = 'none';
      return;
    }
    const img = document.createElement('img');
    img.src = imageUrls[loadedImages];
    img.style.display = 'inline-block'; 
    gallery.appendChild(img);
    loadedImages++;
  }
  gallery.style.overflow = 'auto';
}

loadMoreBtn.addEventListener('click', loadMoreImages);


const cover = document.querySelectorAll('.work-item');

cover.forEach(image => {
  const originalSrc = image.src;
  const hoverSrc = './img/image1.png';
  
  image.addEventListener('mouseover', () => {
    image.src = hoverSrc;
  });
  
  image.addEventListener('mouseleave', () => {
    image.src = originalSrc;
  });
});







const tabs = document.querySelectorAll('.tabs-title');
const content = document.querySelectorAll('.tabs-content li');

tabs.forEach(tab => {
  tab.addEventListener('click', () => {
    tabs.forEach(tab => tab.classList.remove('active'));
    tab.classList.add('active');
    const index = Array.from(tabs).indexOf(tab);
    content.forEach(item => item.style.display = 'none');
    content[index].style.display = 'block';
  });
});



var photoContainers = document.querySelectorAll('.news-box');

photoContainers.forEach(function(container) {
  var date = container.querySelector('.data-box');
  var text = container.querySelector('.news-title');

  container.addEventListener('mouseenter', function() {
    date.classList.add('hover');
    text.classList.add('hover');
  });
  container.addEventListener('mouseleave', function() {
    date.classList.remove('hover');
    text.classList.remove('hover');
  });
});



const allBtn = document.querySelector('[data-category="all"]');
const webBtn = document.querySelector('[data-category="web-design"]');
const graphicBtn = document.querySelector('[data-category="graphic-design"]');
const landingBtn = document.querySelector('[data-category="landing-pages"]');
const wordpressBtn = document.querySelector('[data-category="wordpress"]');

const webImages = document.querySelectorAll('.img-web-design');
const graphicImages = document.querySelectorAll('.img-graphic-design');
const landingImages = document.querySelectorAll('.img-landing-pages');
const wordpressImages = document.querySelectorAll('.img-wordpress');

allBtn.addEventListener('click', () => {
  webImages.forEach(image => image.style.visibility = 'visible');
  graphicImages.forEach(image => image.style.visibility = 'visible');
  landingImages.forEach(image => image.style.visibility = 'visible');
  wordpressImages.forEach(image => image.style.visibility = 'visible');
});

webBtn.addEventListener('click', () => {
  webImages.forEach(image => image.style.visibility = 'visible');
  graphicImages.forEach(image => image.style.visibility = 'hidden');
  landingImages.forEach(image => image.style.visibility = 'hidden');
  wordpressImages.forEach(image => image.style.visibility = 'hidden');
});

graphicBtn.addEventListener('click', () => {
  webImages.forEach(image => image.style.visibility = 'hidden');
  graphicImages.forEach(image => image.style.visibility = 'visible');
  landingImages.forEach(image => image.style.visibility = 'hidden');
  wordpressImages.forEach(image => image.style.visibility = 'hidden');
});

landingBtn.addEventListener('click', () => {
  webImages.forEach(image => image.style.visibility = 'hidden');
  graphicImages.forEach(image => image.style.visibility = 'hidden');
  landingImages.forEach(image => image.style.visibility = 'visible');
  wordpressImages.forEach(image => image.style.visibility = 'hidden');
});

wordpressBtn.addEventListener('click', () => {
  webImages.forEach(image => image.style.visibility = 'hidden');
  graphicImages.forEach(image => image.style.visibility = 'hidden');
  landingImages.forEach(image => image.style.visibility = 'hidden');
  wordpressImages.forEach(image => image.style.visibility = 'visible');
});









const images = document.querySelectorAll(".carousel .images img");const texts = document.querySelectorAll(".text > div");
const textBlocks = document.querySelectorAll(".text div");
const mainImg = document.querySelector(".main-img");
const mainText = document.querySelector(".theHam-text");
const mainName = document.querySelector(".theHam-name");
const mainProfession = document.querySelector(".theHam-profession");
const leftArrow = document.querySelector(".left-arrow");
const rightArrow = document.querySelector(".right-arrow");

let selectedIndex = 0;

function changeImageAndText(index) {
  mainImg.src = images[index].src;
  mainText.textContent = texts[index].querySelector(".theHam-text").textContent;
  mainName.textContent = texts[index].querySelector(".theHam-name").textContent;
  mainProfession.textContent = texts[index].querySelector(".theHam-profession").textContent;

  images[selectedIndex].classList.remove("selected");
  images[index].classList.add("selected");
  selectedIndex = index;
}

leftArrow.addEventListener("click", () => {
  const previousIndex = selectedIndex === 0 ? images.length - 1 : selectedIndex - 1;
  changeImageAndText(previousIndex);
});

rightArrow.addEventListener("click", () => {
  const nextIndex = selectedIndex === images.length - 1 ? 0 : selectedIndex + 1;
  changeImageAndText(nextIndex);
});

images.forEach((image, index) => {
  image.addEventListener("click", () => {
    textBlocks.forEach((block) => {
      block.classList.add("hidden");
      block.classList.remove("active");
    });
    textBlocks[index].classList.add("active");

    mainImg.src = image.src;
  });
});

for (let i = 0; i < images.length; i++) {
  images[i].addEventListener('click', function() {
    mainImg.src = this.src;
    for (let j = 0; j < textBlocks.length; j++) {
      textBlocks[j].classList.add('hidden');
    }
    const textBlock = document.querySelector(`.text${i + 1}`);
    textBlock.classList.remove('hidden');
  });
}

document.querySelector('.arrow.left-arrow').addEventListener('click', function() {
  const currentImg = document.querySelector('.carousel .images img:not(.hidden)');
  currentImg.classList.add('hidden');
  if (currentImg.previousElementSibling) {
    currentImg.previousElementSibling.classList.remove('hidden');
  } else {
    currentImg.parentElement.lastElementChild.classList.remove('hidden');
  }
});






var $grid = $('.gallery-wrapper1').masonry({
  itemSelector: '.item',
  columnWidth: '.grid-sizer',
  gutter: '.gutter-sizer',
  percentPosition: true
});

$grid.imagesLoaded().progress(function() {
  $grid.masonry('layout');
});